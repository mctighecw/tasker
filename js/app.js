// Tasker dynamic to do list app

// define angular app
var app = angular.module('Tasker', []);

// controller
app.controller('TaskController', function($scope, $filter) {
  //localStorage.clear();  //uncomment to clear localStorage
  $scope.today = new Date();
  $scope.saved = localStorage.getItem('taskItems');
  $scope.taskItem = (localStorage.getItem('taskItems') !== null) ? JSON.parse($scope.saved) : [{description: "Sample task", date: null, complete: false}];
  localStorage.setItem('taskItems', JSON.stringify($scope.taskItem));

  $scope.newTask = null;
  $scope.newTaskDate = null;
  $scope.newTaskWith = null;
  $scope.categories = [
    {name: 'Personal'},
    {name: 'Work'},
    {name: 'Other'}
  ];
  $scope.priorities = [
    {
      level: 'High',
      number: 1
    },
    {
      level: 'Medium',
      number: 2
    },
    {
      level: 'Low',
      number: 3
    }
  ];

  // sortBy dropdown menu
  $scope.sortBy = $scope.taskItem.index;

  // add new task
  $scope.newTaskCategory = $scope.categories;
  $scope.newTaskPriority = $scope.priorities;

  $scope.addNew = function () {
    if ($scope.newTask == ('' || null)) {
      $('#description').css({"border": "2px solid #e74c3c", "border-radius": "5px"});
      alert("Please fill out something to do");
      return;
    }
    else {
      $('#description').css("border", "none");
      $scope.taskItem.push({
        description: $scope.newTask,
        date: $scope.newTaskDate,
        complete: false,
        with: $scope.newTaskWith,
        category: $scope.newTaskCategory.name,
        priority: $scope.newTaskPriority
      })
    };
    $scope.newTask = '';
    $scope.newTaskDate = '';
    $scope.newTaskWith = $scope.with;
    $scope.newTaskCategory = $scope.categories;
    $scope.newTaskPriority = $scope.priorities;
    localStorage.setItem('taskItems', JSON.stringify($scope.taskItem));
  };

  // mark all tasks done
  $scope.markAll = function() {
    angular.forEach($scope.taskItem, function (obj) {
      obj.complete = true;
    })
  };

  // unmark all tasks done
  $scope.unmarkAll = function() {
    angular.forEach($scope.taskItem, function (obj) {
      obj.complete = false;
    })
  };

  // delete task
  $scope.deleteTasks = function () {
    var completedTask = $scope.taskItem;
    $scope.taskItem = [];
    angular.forEach(completedTask, function (taskItem) {
      if (!taskItem.complete) {
        $scope.taskItem.push(taskItem);
      }
    });
    localStorage.setItem('taskItems', JSON.stringify($scope.taskItem));
  };

  // save task
  $scope.save = function () {
    localStorage.setItem('taskItems', JSON.stringify($scope.taskItem));
  }

  // print tasks
  $scope.printTasks = function () {
    var today = new Date();
    today = $filter('date')(today, 'd MMM y');
    var printList = document.getElementById('todo-list').innerHTML;
    var popupWindow = window.open('', '_blank');
    popupWindow.document.open();
    popupWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/print.css"></head><body onload="window.print()"><div class="container"><div class="content"><div class="row"><h2>Things To Do</h2>' + '<h4>' + today + '</h4></div>' + printList + '</div></div></body></html>');
    popupWindow.document.close();
  }

  // save tasks to text file
  $scope.saveFileTasks = function () {
    var text = document.getElementById('todo-list').innerText;
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    alert("Saving to_do_list.txt to your default directory");
    saveAs(blob, "to_do_list.txt");
  };

  // e-mail tasks via mail program
  $scope.emailTasks = function () {
    var emailList = document.getElementById('todo-list').innerText;
    window.location.href = "mailto:your@email.com?subject=To%20Do%20List&body=" + emailList;
  };

});