# README

This is a practice project for AngularJS. It is a dynamic to do list webpage.

## App Information

App Name: tasker

Created: August 2016

Updated: November 2018; September 2022

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/tasker)

Production: [Link](https://tasker.mctighecw.site)

## Run with Docker

```
$ docker build -t tasker .
$ docker run --rm -it -p 80:5000 tasker
```

## Notes

- AngularJS version 1.5.8

- Several web tutorials were consulted and some model code used as an example.

- Originally deployed on Heroku

- Moved to personal server in September 2022

Feel free to send me any comments or feedback.

Last updated: 2025-01-05
